<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{

    public function showAllAuthors(Request $request)
    {
        // bô lọc tìm kiếm
        $query = Author::query();
        // Bộ lọc theo tên (name)
        if ($request->has('name')) {
            $query->where('name', 'like', '%' . $request->input('name') . '%');
        }
        // Bộ lọc theo email
        if ($request->has('email')) {
            $query->where('email', 'like', '%' . $request->input('email') . '%');
        }

        // $authors = $query->get();
        // Chọn những trường muốn xuất hiện trong response JSON
        $authors = $query->select('id', 'name', 'email', 'location')->get();

        //Chỉnh sửa hoặc thêm trường mới
        $modifiedAuthors = $authors->map(function ($author) {
            // Chỉnh sửa giá trị hoặc thêm trường mới
            // $author['name'] = 'Chien';
            $author['phone'] = '0377683838';

            return $author;
        });

        return response()->json($modifiedAuthors);
    }

    public function showOneAuthor($id)
    {
        return response()->json(Author::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:authors',
            'location' => 'required|alpha'
        ]);
        // đã kiểm tra API POST với Postman.

        $author = Author::create($request->all());

        // response()->json() trả về phản hồi có dạng Json
        // 201 Mã HTTP cho biết tài nguyên mới được tạo ra
        return response()->json($author, 201);
    }

    public function update($id, Request $request)
    {
        $author = Author::findOrFail($id);
        $author->update($request->all());

        // 200 Mã HTTP cho biết yêu cầu đã thành công
        return response()->json($author, 200);
    }

    public function delete($id)
    {
        Author::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function search(array $input = [])
    {
    }
}
